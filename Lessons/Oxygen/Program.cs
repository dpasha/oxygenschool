﻿using Oxygen.lesson_01;

namespace Oxygen
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Lesson_01.Run();
        }
    }
}
